# coding: utf-8

from xml_generator.generator import BaseGenerator


class SimplyGenerator(BaseGenerator):

    pass


tree = SimplyGenerator()
tree.add({
    'foo': 'bar',
    'bar': 'baz',
})

tree.save('/tmp/1.xml')