# coding: utf-8
import re

from xml.etree import cElementTree as ET


class BaseGenerator(object):

    default_exp = '$'

    object_node_name = 'object'

    fields = {}

    def __init__(self):

        # создать пустое дерево
        root, parent = self._get_empty_tree()
        self.root = root
        self.parent_element = parent

    def add(self, object_data):
        """
        add new object to existing object list
        """

        object_node = self._create_object_node()

        for key, value in object_data.items():
            self._build_field(object_node, key, value)

        self.parent_element.append(object_node)

    def save(self, filename):

        with open(filename, 'wb') as f:
             f.write(ET.tostring(self.root))

    def _get_empty_tree(self):
        root_node = ET.fromstring('<root></root>')
        return root_node, root_node

    node_and_attr_regexp = re.compile(r'([\w\-\_]+)&')
    @classmethod
    def _get_handlers(cls, key, value):

        handler_exp = cls.fields.get(key) or cls.default_exp

        # набор хэндлеров
        handlers = []

        # части выражения по которым будем получать хендлеры
        exp_parts = handler_exp.split('/')

        for part in exp_parts:

            # нужно просто вставить структуру <key>value</key> ?
            if part == '$':

                handler = {
                    'func': cls._node_with_value,
                    'kwargs': {
                        'node_name': key,
                        'node_text': value,
                    }
                }

            # нужно добавить к ноде аттрибут
            elif part == '&':

                handler = {
                    'func': cls._simple_attribute,
                    'kwargs': {
                        'attr_name': key,
                        'attr_value': value,
                    }
                }

            # просто создать ноду
            elif part == '@':

                handler = {
                    'func': cls._simple_node,
                    'kwargs': {
                        'node_name': key,
                    }
                }

            # создать ноду и к ней добавить аттрибут
            else:

                node_with_attr = cls.node_and_attr_regexp.findall(part)
                if len(node_with_attr) == 1:
                    handler = {
                        'func': cls._node_with_attr,
                        'kwargs': {
                            'node_name': node_with_attr[0],
                            'attr_name': key,
                            'attr_value': value,
                        }
                    }

                else:

                    handler = {
                        'func': cls._simple_node,
                        'kwargs': {
                            'node_name': part,
                        }
                    }

            handlers.append(handler)
        return handlers

    @classmethod
    def _build_field(cls, object_node, key, value):

        if isinstance(value, dict):

            # создать ноду и вложить в нее субноды
            new_node = ET.Element(key)
            object_node.append(new_node)

            for subkey, subvalue in value.items():
                cls._build_field(new_node, subkey, subvalue)

        else:
            value = cls._normalize_value(value)
            handlers = cls._get_handlers(key, value)

            node = object_node
            for handler in handlers:

                func = handler['func']
                kwargs = handler.get('kwargs')
                node = func(node, kwargs=kwargs)

    @staticmethod
    def _simple_node(node, kwargs):

        # создать ноду
        new_node = ET.Element(kwargs.get('node_name'))
        node.append(new_node)
        return new_node

    @staticmethod
    def _node_with_value(node, kwargs):

        # создать ноду со значением
        new_node = ET.Element(kwargs.get('node_name'))
        new_node.text = kwargs.get('node_text')
        node.append(new_node)

    @staticmethod
    def _simple_attribute(node, kwargs):

        # добавить к ноде аттрибут="значение"
        node.set(kwargs.get('attr_name'), kwargs.get('attr_value'))
        return node

    @staticmethod
    def _node_with_attr(node, kwargs):

        # создать ноду и к ней добавить аттрибут
        new_node = ET.Element(kwargs.get('node_name'))
        new_node.set(kwargs.get('attr_name'), kwargs.get('attr_value'))
        node.append(new_node)
        return new_node

    @classmethod
    def _create_object_node(cls):

        return ET.Element(cls.object_node_name)

    @staticmethod
    def _normalize_value(value):
        return str(value)

